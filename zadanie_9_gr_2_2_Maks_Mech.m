% some comment: My idea on this task was to count pulses (peaks) by reading
% them from histogram and estimate their number. I assume that number of
% signals below 0 after clearing noise, sholud be same as over 0. But it
% depends on file - data must be very close to normal distribution, so for
% data in file Co60_1350V_x20_p7.dat this estimation can't be done in a
% just simply way. 

% clearing console & values
clear;
clc;

% how to read GB size file inspired on code authored by Jacek Pawlyta

% opening file
myFileName = 'Co60_1350V_x20_p7.dat';
myFile = dir(myFileName);
myFileId=fopen(myFileName);

blockSize = 10000000;
screenSize = 2000;
fresh = 1; % value 0 or 1, later about it

% in this loop I count each valuables for histogram
for k = 1:fix(myFile.bytes/blockSize)+1
    data = fread(myFileId,blockSize,'uint8');
    valuables = unique(data);
    range = size(valuables);
    % outcome is array with all data counted in, but firstly we need to
    % crate it but only for the first time
    if fresh == 1;
        outcome(1,1) = valuables(1);
        outcome(1,2) = 0;
        fresh = 0; % changing value
    end;
    % filling array with values
    for i = 1:range(1);
        if ismember(valuables(i),outcome(:,1)) == 0;
            range2 = size(outcome);
            outcome(range2(1)+1,1) = valuables(i);
            outcome(range2(1)+1,2) = [0];
        end;
    end;
    range2 = size(outcome);
    errorfix1 = size(data); % at very EOF data can be less than blockSize
    % for i = 1:blockSize; % there was an error
    for i = 1:errorfix1(1);
        for j = 1:range2(1);
            if outcome(j,1) == data(i);
                outcome(j,2) = outcome(j,2)+1;
            end;    
        end;
    end;
    % sorting array by rows
    outcome = sortrows(outcome,1);
    outcome(); % line for checking in beta version
end;

% closing file after reading
fclose(myFileId);

% ploting histogram with reducted noise
output = NoiseLevel(outcome);
bar(output(:,1),output(:,2))
xlabel('values from sensor (noise already reducted)');
ylabel('number of occurrences');
title('Histogram');

% calculating estimation
estimation = Circa(output);

% saving plot as PDF file
saveas(gcf,'histogram.pdf');

% calculating value of noise
noise_value = Noise(outcome);

% so to check my idea of estimation, we check it with code scripted by Adam
% Stasiak, which I adapated for this project. Lines from 81 to 87 are based
% on code by Adam Stasiak, which can be found there:
% https://gitlab.com/matlab-applications-in-physic/zadanie_9_gr_2_2_adsta26
pulses = 0;
fd = fopen('Co60_1350V_x20_p7.dat', 'r');
while ~feof(myFileId)
	dataBuff = fread(myFileId, blockSize, 'uint8');
	pulses = pulses + CountPulses(dataBuff, noise_value);
	clear dataBuff;
end

% writing results of calcuation to .dat file
file = fopen([myFileName, '_peak_analysis_results.dat'] ,'w+');
fprintf(file, 'Noise value: %d \n', noise_value);
val = estimation(1);
fprintf(file, 'Estimation of pulses: %s \n', val);
comment = estimation(2);
fprintf(file, 'Estimation comment: %s \n', comment);
fprintf(file, 'Number of pulses: %d \n', pulses);
unc = round(sqrt(pulses));
% here I calculate simply test for matching values
est_test = abs((pulses - double(estimation(1))/unc));
if est_test > 3;
    test_comment = 'this aproximation for this data is worth nothing';
end
if est_test <= 3;
    test_comment = 'this aproximation may be good for this data';
end
fprintf(file, 'Estimation test: %s \n', test_comment);
fclose(file);

% function for reducting noise to plot this on bar plot
function noise = NoiseLevel(matrix)
    noise_find = max(matrix(:,2));
    gotg = size(matrix);
    for i = 1:gotg(1);
        if matrix(i,2) == noise_find;
            stop = i;
        end;
    end;
    noiselevel = matrix(stop,1);
    
    % reducting noise
    for i = 1:gotg(1);
        noise(i,1) = matrix(i,1) - noiselevel;
        noise(i,2) = matrix(i,2);
    end;
end

% function for reducting noise to apply this in CountPulses function
function noise_values = Noise(matrix)
    noise_find = max(matrix(:,2));
    gotg = size(matrix);
    for i = 1:gotg(1);
        if matrix(i,2) == noise_find;
            stop = i;
        end;
    end;
    noise_values = matrix(stop,1);
end

% main simply estimation function
function estimation = Circa(array)
    %setting bounds
    low = array(1,1);
    high = - low;
    % calculating average in bounds
    sum = 0;
    count = 0;
    barrier = 2*high+1;
    for i = 1:barrier;
        sum = sum + array(i,1) * array(i,2);
        count = count + array(i,2);
    end;
    % in normal distribution average ought to be 0
    average = sum/count;
    % getting numbers of peaks
    gotg2 = size(array);
    peaks = 0;
    for i = barrier:gotg2(1);
        peaks = peaks + array(1,2);
    end;
    % average test, I assumed that near 0 in normal distribution is 0.1 
    test = 'estimation may be good';
    if average > 0.1;
       test = 'peaks may be underestimated'; 
    end
    if average < -0.1;
       test = 'peaks may be overestimated'; 
    end
    estimation = [peaks,string(test)];
end

% so as I wrote earlier to check my idea I used function CountPulses (lines
% from 174 to 186) authored by Adam Stasiak, which can be found there: 
% https://gitlab.com/matlab-applications-in-physic/zadanie_9_gr_2_2_adsta26
function pulses = CountPulses(data, noise_value)
    data = nonzeros(diff(data - noise_value)); %derivative of (data - noise level) with constant values ??removed
    pulses = 0;
    i = 1;
    while(i < size(data, 1) - 6)
      if (sum(data(i:i+2)) > 2 && sum(data(i+3:i+5)) < 2);
        pulses = pulses + 1;
      elseif (sum(data(i:i+2)) < 2 && sum(data(i+3:i+5)) > 2);
        pulses = pulses - 1;
      end
      i = i + 6;
    end
end